import OpenGL.GL as gl
import OpenGL.GLU as glu
import OpenGL.GLUT as glut

class shadowMapping(object):
    def __init__ (self, resolution=512):
        self.shadowMapTexture = None
        self.shadowMapSize = resolution
        
        self.textureTrasnformS = [0,0,0,0]
        self.textureTrasnformT = [0,0,0,0]
        self.textureTrasnformR = [0,0,0,0]
        self.textureTrasnformQ = [0,0,0,0]
        
    def createDepthTexture(self):
        #Create the shadow map texture
        self.shadowMapTexture = gl.glGenTextures(1)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.shadowMapTexture)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_EDGE)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_DEPTH_COMPONENT, self.shadowMapSize, self.shadowMapSize, 0, gl.GL_DEPTH_COMPONENT, gl.GL_UNSIGNED_BYTE, None)

        #Enable shadow comparison
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_COMPARE_MODE, gl.GL_COMPARE_R_TO_TEXTURE)

        #Shadow comparison should be true (ie not in shadow) if r<=texture
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_COMPARE_FUNC, gl.GL_LEQUAL)

        #Shadow comparison should generate an INTENSITY result
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_DEPTH_TEXTURE_MODE, gl.GL_INTENSITY)

    def loadTextureTransform(self):
        lightProjectionMatrix = gl.glGetFloatv(gl.GL_PROJECTION_MATRIX)
        lightViewMatrix = gl.glGetFloatv(gl.GL_MODELVIEW_MATRIX)

        #Busca as matrizes de view e projection da luz
        #gl.glGetFloatv(gl.GL_PROJECTION_MATRIX, lightProjectionMatrix)
        #gl.glGetFloatv(gl.GL_MODELVIEW_MATRIX, lightViewMatrix)

        #Salva o estado da matrix mode.
        gl.glPushAttrib(gl.GL_TRANSFORM_BIT)
        gl.glMatrixMode(gl.GL_TEXTURE)
        gl.glPushMatrix()

        #Calculate texture matrix for projection
        #This matrix takes us from eye space to the light's clip space
        #It is postmultiplied by the inverse of the current view matrix when specifying texgen
        biasMatrix = [0.5, 0.0, 0.0, 0.0,
                      0.0, 0.5, 0.0, 0.0,
                      0.0, 0.0, 0.5, 0.0,
                      0.5, 0.5, 0.5, 1.0]
        #bias from [-1, 1] to [0, 1]

        #Aplica as 3 matrizes em uma só, levando um fragmento em 3D para o espaço
        #canônico da câmera.
        gl.glLoadMatrixf(biasMatrix)
        gl.glMultMatrixf(lightProjectionMatrix)
        gl.glMultMatrixf(lightViewMatrix)
        #gl.glGetFloatv(gl.GL_TEXTURE_MATRIX, textureMatrix)
        textureMatrix = gl.glGetFloatv(gl.GL_TEXTURE_MATRIX)

        #Separa as colunas em arrays diferentes por causa da opengl
        for i in range(4):
            self.textureTrasnformS[i] = textureMatrix[i][0]
            self.textureTrasnformT[i] = textureMatrix[i][1]
            self.textureTrasnformR[i] = textureMatrix[i][2]
            self.textureTrasnformQ[i] = textureMatrix[i][3]

        gl.glPopMatrix()
        gl.glPopAttrib()

    def enableDepthCapture(self):
            #Protege o código anterior a esta função
            gl.glPushAttrib(gl.GL_ENABLE_BIT | gl.GL_TEXTURE_BIT | gl.GL_LIGHTING_BIT | gl.GL_VIEWPORT_BIT | gl.GL_COLOR_BUFFER_BIT)

            #Se a textura ainda não tiver sido criada, crie
            if self.shadowMapTexture:
                gl.glDeleteTextures(1,self.shadowMapTexture)
                self.shadowMapTexture = None
            self.createDepthTexture()

            #Seta a viewport com o mesmo tamanho da textura.
            #O tamanho da viewport não pode ser maior que o tamanho da tela.
            #SE for, deve-se usar offline rendering e FBOs.
            gl.glViewport(0, 0, self.shadowMapSize, self.shadowMapSize)

            #Calcula a transformação do espaço de câmera para o espaço da luz
            #e armazena a transformação para ser utilizada no teste de sombra do rendering
            self.loadTextureTransform()

            #Habilita Offset para evitar flickering.
            #Desloca o mapa de altura 1.9 vezes + 4.00 para trás.
            gl.glPolygonOffset(1.9, 4.00)
            gl.glEnable(gl.GL_POLYGON_OFFSET_FILL)

            #Flat shading for speed
            gl.glShadeModel(gl.GL_FLAT)
            #Disable Lighting for performance.
            gl.glDisable(gl.GL_LIGHTING)
            #Não escreve no buffer de cor, apenas no depth
            gl.glColorMask(0, 0, 0, 0)

    def disableDepthCapture(self):
        #Copia o Depth buffer para a textura.
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.shadowMapTexture)

        #SubTexture não realoca a textura toda, como faz o texture
        gl.glCopyTexSubImage2D(gl.GL_TEXTURE_2D, 0, 0, 0, 0, 0, self.shadowMapSize, self.shadowMapSize)

        #Limpa o Buffer de profundidade
        gl.glClear(gl.GL_DEPTH_BUFFER_BIT)

        #Retorna as configurações anteriores ao depthCapture
        gl.glPopAttrib()

    def enableShadowTest(self):
        #Protege o código anterior a esta função
        gl.glPushAttrib(gl.GL_TEXTURE_BIT | gl.GL_ENABLE_BIT)

        #Habilita a geração automática de coordenadas de textura do ponto de vista da câmera
        gl.glTexGeni(gl.GL_S, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR)
        gl.glTexGeni(gl.GL_T, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR)
        gl.glTexGeni(gl.GL_R, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR)
        gl.glTexGeni(gl.GL_Q, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR)

        #Aplica a transformação destas coordenadas para o espaço da luz
        gl.glTexGenfv(gl.GL_S, gl.GL_EYE_PLANE, self.textureTrasnformS)
        gl.glTexGenfv(gl.GL_T, gl.GL_EYE_PLANE, self.textureTrasnformT)
        gl.glTexGenfv(gl.GL_R, gl.GL_EYE_PLANE, self.textureTrasnformR)
        gl.glTexGenfv(gl.GL_Q, gl.GL_EYE_PLANE, self.textureTrasnformQ)

        #Ativa
        gl.glEnable(gl.GL_TEXTURE_GEN_S)
        gl.glEnable(gl.GL_TEXTURE_GEN_T)
        gl.glEnable(gl.GL_TEXTURE_GEN_R)
        gl.glEnable(gl.GL_TEXTURE_GEN_Q)

        #Bind & enable shadow map texture
        gl.glEnable(gl.GL_TEXTURE_2D)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.shadowMapTexture)

    def disableShadowTest(self):
        #Retorna as configurações anteriores do programa
        gl.glPopAttrib()

    #ShadowMapping(int resolution = 512) {
            #shadowMapTexture = NULL;
            #shadowMapSize = resolution;
    #}
    #virtual ~ShadowMapping() {
            #shadowMapTexture = NULL;
            #gl.glDeleteTextures(1, &shadowMapTexture);
    #}
#};
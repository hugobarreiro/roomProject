import OpenGL.GL as gl
import OpenGL.GLU as glu
import OpenGL.GLUT as glut

import concurrent.futures

class Room(object):
    def __init__(self):
        self.Xside = 3.0
        self.Yside = 3.0
        self.Zside = 1.5
        self.squares = 5
        
    def draw(self):
        gl.glPushMatrix()
        gl.glBegin(gl.GL_QUADS)
        
        #with concurrent.futures.ProcessPoolExecutor() as pool:
        #    [pool.map(self._drawWalls, range(-self.squares,self.squares), range(-self.squares,self.squares))]
        [self._drawWalls(i, j) for i in range(-self.squares,self.squares) for j in range(-self.squares,self.squares)]
        
        gl.glEnd()
        gl.glPopMatrix()
        
    def _drawWalls(self, i, j):
        #draw front wall
        gl.glNormal3f(-1.0, 0.0, 0.0)
        gl.glColor3f(1.0, 1.0, 1.0)
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_AMBIENT_AND_DIFFUSE, [1.0, 1.0, 1.0, 1.0] )
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_SPECULAR,  [1.0, 1.0, 1.0, 1.0] )
        gl.glMaterialf  (gl.GL_FRONT, gl.GL_SHININESS, 25.0)
        gl.glVertex3f(self.Xside,  i   *self.Yside/self.squares,  j   *self.Zside/self.squares)
        gl.glVertex3f(self.Xside,  i   *self.Yside/self.squares, (j+1)*self.Zside/self.squares)
        gl.glVertex3f(self.Xside, (i+1)*self.Yside/self.squares, (j+1)*self.Zside/self.squares)
        gl.glVertex3f(self.Xside, (i+1)*self.Yside/self.squares,  j   *self.Zside/self.squares)
        
        #draw back wall
        gl.glNormal3f(1.0, 0.0, 0.0)
        gl.glColor3f(0.0, 1.0, 1.0)
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_AMBIENT_AND_DIFFUSE, [0.0, 1.0, 1.0, 1.0] )
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_SPECULAR,  [0.0, 0.0, 0.0, 1.0] )
        gl.glMaterialf  (gl.GL_FRONT, gl.GL_SHININESS, 25.0)
        gl.glVertex3f(-self.Xside,  i   *self.Yside/self.squares,  j   *self.Zside/self.squares)
        gl.glVertex3f(-self.Xside,  i   *self.Yside/self.squares, (j+1)*self.Zside/self.squares)
        gl.glVertex3f(-self.Xside, (i+1)*self.Yside/self.squares, (j+1)*self.Zside/self.squares)
        gl.glVertex3f(-self.Xside, (i+1)*self.Yside/self.squares,  j   *self.Zside/self.squares)
    
        #draw ceiling
        gl.glNormal3f(0.0, 0.0, -1.0)
        gl.glColor3f(0.0, 1.0, 0.0)
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_AMBIENT_AND_DIFFUSE, [0.0, 1.0, 0.0, 1.0] )
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_SPECULAR,  [0.0, 0.0, 0.0, 1.0] )
        gl.glMaterialf  (gl.GL_FRONT, gl.GL_SHININESS, 25.0)
        gl.glVertex3f( i   *self.Xside/self.squares,  j   *self.Yside/self.squares, self.Zside)
        gl.glVertex3f( i   *self.Xside/self.squares, (j+1)*self.Yside/self.squares, self.Zside)
        gl.glVertex3f((i+1)*self.Xside/self.squares, (j+1)*self.Yside/self.squares, self.Zside)
        gl.glVertex3f((i+1)*self.Xside/self.squares,  j   *self.Yside/self.squares, self.Zside)
    
        #draw floor
        gl.glNormal3f(0.0, 0.0, 1.0)
        gl.glColor3f(0.0, 0.0, 1.0)
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_AMBIENT_AND_DIFFUSE, [0.0, 0.0, 1.0, 1.0] )
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_SPECULAR,  [0.0, 0.0, 0.0, 1.0] )
        gl.glMaterialf  (gl.GL_FRONT, gl.GL_SHININESS, 25.0)
        gl.glVertex3f( i   *self.Xside/self.squares,  j   *self.Yside/self.squares, -self.Zside)
        gl.glVertex3f( i   *self.Xside/self.squares, (j+1)*self.Yside/self.squares, -self.Zside)
        gl.glVertex3f((i+1)*self.Xside/self.squares, (j+1)*self.Yside/self.squares, -self.Zside)
        gl.glVertex3f((i+1)*self.Xside/self.squares,  j   *self.Yside/self.squares, -self.Zside)
    
        #draw right wall
        gl.glNormal3f(0.0, -1.0, 0.0)
        gl.glColor3f(1.0, 1.0, 0.0)
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_AMBIENT_AND_DIFFUSE, [1.0, 1.0, 0.0, 1.0] )
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_SPECULAR,  [0.0, 0.0, 0.0, 1.0] )
        gl.glMaterialf  (gl.GL_FRONT, gl.GL_SHININESS, 25.0)
        gl.glVertex3f( i   *self.Xside/self.squares, self.Yside,  j   *self.Zside/self.squares)
        gl.glVertex3f( i   *self.Xside/self.squares, self.Yside, (j+1)*self.Zside/self.squares)
        gl.glVertex3f((i+1)*self.Xside/self.squares, self.Yside, (j+1)*self.Zside/self.squares)
        gl.glVertex3f((i+1)*self.Xside/self.squares, self.Yside,  j   *self.Zside/self.squares)
    
        #draw left wall
        gl.glNormal3f(0.0, 1.0, 0.0)
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_AMBIENT_AND_DIFFUSE, [1.0, 0.0, 0.0, 1.0] )
        gl.glMaterialfv (gl.GL_FRONT, gl.GL_SPECULAR,  [0.0, 0.0, 0.0, 1.0] )
        gl.glMaterialf  (gl.GL_FRONT, gl.GL_SHININESS, 25.0)
        gl.glVertex3f( i   *self.Xside/self.squares, -self.Yside,  j   *self.Zside/self.squares)
        gl.glVertex3f( i   *self.Xside/self.squares, -self.Yside, (j+1)*self.Zside/self.squares)
        gl.glVertex3f((i+1)*self.Xside/self.squares, -self.Yside, (j+1)*self.Zside/self.squares)
        gl.glVertex3f((i+1)*self.Xside/self.squares, -self.Yside,  j   *self.Zside/self.squares)
